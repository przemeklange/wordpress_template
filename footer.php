<?php wp_footer() ?>
<footer>
    <div class="container padtop">
        <div class="row">
            <div class="col-lg-3">
                <img src="<?php bloginfo('template_url'); ?>/img/morebiglogo.png" alt="logo" />
                <p style="padding-top: 15px;">Lorem ipsum dolor sit amet,Stet clita kasd gubergren, no sea takimata </p>
            </div>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="sentmail">
                            <p class="upinmail">Sent Email</p>
                            <p class="mail">kontakt@przemyslawlange.pl</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="sentmail">
                            <p class="upinmail">Make Call</p>
                            <p class="mail">+48 514 245 872</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="sentmail">
                            <p class="upinmail">get in touch</p>
                            <p class="mail">Polska, Poznań</p>
                        </div>
                    </div>

                </div>



            </div>
        </div>
    </div>
    <div class="row">
        <hr class="new5">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 d-flex justify-content-center padb">
                2021 Przemysław Lange
            </div>
        </div>
    </div>

</footer>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>