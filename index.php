<?php get_header() ?>

<div class="mojebg ">

  <div class="text-wrap">
    <img class="abs pos" src="<?php bloginfo('template_url'); ?>/img/path1.png" alt="logo" />
    <img class="abs pos" src="<?php bloginfo('template_url'); ?>/img/path2.png" alt="logo" />
    <img class="abs pos" src="<?php bloginfo('template_url'); ?>/img/path3.png" alt="logo" />
    <img class="abs pos" src="<?php bloginfo('template_url'); ?>/img/path4.png" alt="logo" />
    <img class="abs pos" src="<?php bloginfo('template_url'); ?>/img/path5.png" alt="logo" />
    <img class="abs pos" src="<?php bloginfo('template_url'); ?>/img/path7.png" alt="logo" />
    <img class="abs pos" src="<?php bloginfo('template_url'); ?>/img/path8.png" alt="logo" />
  </div>

</div>

<a href="#top"><img src="<?php bloginfo('template_url'); ?>/img/next.svg" alt="logo" id="arrow" /></a>



<div class="container d-flex justify-content-center flex-column height align-middle">

  <a href="javascript:;" class="circle-btn"></a>
  <div class="row">
    <div class="col-lg-7 d-flex flex-column justify-content-center align-middle sliderod">
      <h1 class="inhead"><?php the_field('title_head'); ?></h1>
      <p><?php the_field('paragraph_in_header'); ?></p>
      <div class="d-flex">
        <a id="linkhead" href="<?php the_field('linkpage'); ?>" target="_blank"><?php the_field('text_linkowania'); ?></a>
        <a id="linkheadtwo" href="<?php the_field('linkpage'); ?>" target="_blank">See Live Damo</a>
      </div>
    </div>
    <div class="col-lg-5 align-middle">
      <img class="imgslider" src="<?php bloginfo('template_url'); ?>/img/pani.png" alt="logo" />
    </div>
  </div>
</div>
<div class="container d-flex justify-content-center flex-column" id="about">
  <div class="row">
    <div class="col-sm-6 col-lg-6">
      <div class="photography">
        <img class="photoinabout" src="<?php bloginfo('template_url'); ?>/img/writetwo.png" alt="logo" />
      </div>
    </div>
    <div class="col-lg-6 d-flex flex-column justify-content-center">
      <div class="minitxt">About Me</div>
      <h2 class="aboutme"><?php the_field('about_me_title'); ?></h2>
      <div class="about">
        <?php the_field('about_me_textarea'); ?>

      </div>
      <?php if (get_field('filecv')) : ?>
        <a class="btn__click" href="<?php the_field('filecv'); ?>">Download CV</a>
      <?php endif; ?>

    </div>
  </div>
</div>
<div class="container padtop" id="services">
  <div class="row">
    <div class="col-lg-12">
      <div class="title_services d-flex flex-column align-items-center">
        <h2>My Services</h2>
        <p>Our Provided Features & Expertise</p>
      </div>
    </div>
  </div>
  <div class="row d-flex flex-row align-items-center justify-content-center padtop30">
    <div class="col-lg-6">
      <div class="whitespaceone d-flex flex-column align-items-start justify-content-center">
        <img src="<?php bloginfo('template_url'); ?>/img/icoone.png" alt="logo" />
        <h2>Web Design</h2>
        <img src="<?php bloginfo('template_url'); ?>/img/line.png" alt="logo" />
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos velit ullam eos assumenda dolorem ipsam quia laborum eius quibusdam iste numquam, temporibus consectetur suscipit dolor harum porro animi, perspiciatis quo.</p>
        <button>Read more</button>
      </div>
    </div>
    <div class="col-lg-6 whitespaceone d-flex flex-column align-items-start justify-content-center">
      <h1><?php the_field('two_progress_bar_percentage') ?>%</h1>
      <progress max="100" value="<?php the_field('two_progress_bar_percentage'); ?>"></progress><br />
      <h2>Web Design</h2>
      <p>HTML, CSS, jQuery</p>
    </div>
  </div>
  <div class="row d-flex flex-row align-items-center justify-content-center padtop">

    <div class="col-lg-6 whitespaceone d-flex flex-column align-items-start justify-content-center">
      <h1><?php the_field('first_circle_percentage'); ?>%</h1>
      <progress max="100" value="<?php the_field('first_circle_percentage'); ?>"></progress><br />
      <h2>Graphic Design</h2>
      <p>Photoshop, Illustrator</p>
    </div>
    <div class="col-lg-6 d-flex flex-column align-items-center justify-content-center">
      <div class="greenspace whitespaceone d-flex flex-column align-items-start justify-content-center ">
        <img src="<?php bloginfo('template_url'); ?>/img/pedzel.png" alt="logo" />
        <h2>Web Design</h2>
        <img src="<?php bloginfo('template_url'); ?>/img/white_line.png" alt="logo" />
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos velit ullam eos assumenda dolorem ipsam quia laborum eius quibusdam iste numquam, temporibus consectetur suscipit dolor harum porro animi, perspiciatis quo.</p>
        <button>Read more</button>
      </div>
    </div>
  </div>

</div>

<div class="container padtop" id="portfolio">
  <div class="row">
    <div class="col-lg-12">
      <div class="title_services d-flex flex-column align-items-center">
        <h2>Portfolio</h2>
        <p>My Work Examples</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="lg-col-12">
      <div class="gallery">
        <figure>
          <img src="<?php bloginfo('template_url'); ?>/img/todd-steitle-ExnAdmi-Asc-unsplash-2.jpg" alt="logo" />
          <figcaption>Daytona Beach <small>United States</small></figcaption>
        </figure>
        <figure>
          <img src="<?php bloginfo('template_url'); ?>/img/onur-binay-_RpPMkqTTTg-unsplash.jpg" alt="logo" />
          <figcaption>Териберка, gorod Severomorsk <small>Russia</small></figcaption>
        </figure>
        <figure>
          <img src="<?php bloginfo('template_url'); ?>/img/alexandar-todov-Im8ylpB8SpI-unsplash.jpg" alt="logo" />
          <figcaption>
            Bad Pyrmont <small>Deutschland</small>
          </figcaption>
        </figure>

      </div>

      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="display:none;">
        <symbol id="close" viewBox="0 0 18 18">
          <path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M9,0.493C4.302,0.493,0.493,4.302,0.493,9S4.302,17.507,9,17.507
			S17.507,13.698,17.507,9S13.698,0.493,9,0.493z M12.491,11.491c0.292,0.296,0.292,0.773,0,1.068c-0.293,0.295-0.767,0.295-1.059,0
			l-2.435-2.457L6.564,12.56c-0.292,0.295-0.766,0.295-1.058,0c-0.292-0.295-0.292-0.772,0-1.068L7.94,9.035L5.435,6.507
			c-0.292-0.295-0.292-0.773,0-1.068c0.293-0.295,0.766-0.295,1.059,0l2.504,2.528l2.505-2.528c0.292-0.295,0.767-0.295,1.059,0
			s0.292,0.773,0,1.068l-2.505,2.528L12.491,11.491z" />
        </symbol>
      </svg>

    </div>
  </div>
</div>
<div class="container padtop" id="contact">
  <div class="row">
    <div class="col-lg-12">
      <img class="abs map" src="<?php bloginfo('template_url'); ?>/img/map__contact.png" alt="logo" />
      <div class="title_services d-flex flex-column align-items-center">
        <h2>Contact me</h2>
        <p>Send Us a Message</p>
      </div>
    </div>
    <div class="col-lg-12">

      <div class="row d-flex flex-column align-items-center">
        <form class="col-lg-8">
          <div class="form-row">
            <div class="form-group col-md-6">
              <input type="text" class="form-control" id="inputEmail4" placeholder="Name">
            </div>
            <div class="form-group col-md-6">
              <input type="email" class="form-control" id="inputPassword4" placeholder="E-mail">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <input type="text" class="form-control" id="inputPhone" placeholder="Phone">
            </div>
            <div class="form-group col-md-6">
              <input type="text" class="form-control" id="inputBudget" placeholder="Budget">

            </div>
            <div class="form-group col-md-12">
              <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Message" rows="3"></textarea>
            </div>
            <div class="col-lg-12 d-flex justify-content-center">
              <button type="submit" class="btn sendmess">Send Message</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    var parallaxItem = $('.text-wrap')[0];

    $('.mojebg').mousemove(function(e) {
      parallax(e, parallaxItem);
    });

    function parallax(e, target) {

      var x = ($(window).width() - target.offsetWidth) / 2 - (e.pageX - ($(window).width() / 2)) / 20;
      var y = ($(window).height() - target.offsetHeight) / 2 - (e.pageY - ($(window).height() / 2)) / 20;

      $(target).offset({
        top: y,
        left: x,
      });
    };
  });

  popup = {
    init: function() {
      $('figure').click(function() {
        popup.open($(this));
      });

      $(document).on('click', '.popup img', function() {
        return false;
      }).on('click', '.popup', function() {
        popup.close();
      })
    },
    open: function($figure) {
      $('.gallery').addClass('pop');
      $popup = $('<div class="popup" />').appendTo($('body'));
      $fig = $figure.clone().appendTo($('.popup'));
      $bg = $('<div class="bg" />').appendTo($('.popup'));
      $close = $('<div class="close"><svg><use xlink:href="#close"></use></svg></div>').appendTo($fig);
      $shadow = $('<div class="shadow" />').appendTo($fig);
      src = $('img', $fig).attr('src');
      $shadow.css({
        backgroundImage: 'url(' + src + ')'
      });
      $bg.css({
        backgroundImage: 'url(' + src + ')'
      });
      setTimeout(function() {
        $('.popup').addClass('pop');
      }, 10);
    },
    close: function() {
      $('.gallery, .popup').removeClass('pop');
      setTimeout(function() {
        $('.popup').remove()
      }, 100);
    }
  }

  popup.init()
</script>




<?php get_footer() ?>